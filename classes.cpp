#include <iostream>

//Se usa namespace para no tener que teclear std antes de cada funcion.
//Solo se usa por novatos y para prototipado, en produccion te piden que
//uses std::<FUNCION>. NO SE TE OLVIDE.
using namespace std;

class ClaseUno{
	//Funciones que solo pueden usarse adentro de la clase.
	private:
		int sumaNumeros(int x, int y){
			int respuesta = x + y;
			return respuesta;
		}
	//Funciones que pueden ser usadas fuera de la clase.
	public:
		void funcionPublica(){
			cout << "Soy una funcion publica" << endl;
		}
		void imprimirSuma(){
			cout << "La suma de 2 + 3 es: " << sumaNumeros(2,3) << endl;
		}
		//Esta es una clase que recibe y retorna parametros.
		int restaNumero(int x, int y){
			int respuesta = x - y;
			return respuesta;
		}
};

int main(){
	//cout se utiliza para enviar mensajes a consola.
	//endl es el salto de linea en C++.
	cout<<"Soy el programa prueba. Tema: Clases"<<endl;

	//Declaro un objeto de tipo ClaseUno llamado objetoUno.
	ClaseUno objetoUno;

	//Ahora invocamos la funcion funcionPublica para que haga lo suyo.
	objetoUno.funcionPublica();

	//Ahora invocamos una funcion imprimirSuma que suma dos numeros
	//pero lo hace llamando a otra clase privada llamada sumaNumeros.
	//Si tratas de usar sumaNumeros aqui, te va a marcar error porque
	//es privada, por tanto esta restringida.
	objetoUno.imprimirSuma();

	int resta = objetoUno.restaNumero(3,2);
	cout<<"La resta de 3 - 2 es: "<<resta<<endl;

	//Si descomentas esto veras que te da error.
	//objetoUno.sumaNumeros(2,2);
	return 0;
}