#include <iostream>
#include <string>
using namespace std;

class GetsetClass{
	private:
		string hola;

	public:
		//Asigna un valor a la variable privada hola.
		void setValue(string val){
			hola = val;
		}
		//Obtiene el valor de la variable privada hola.
		string getValue(){
			return hola;
		}
};

int main(){
	cout << "Hola soy el tercer tutorial de C++. Tema: getters y setters" <<endl;
	//Creamos un objeto de tipo getsetClass.
	GetsetClass objetoUno;

	//Le asignamos un valor a la variable privada hola usando el metodo setter.
	objetoUno.setValue("Soy una variable privada");

	//Obtenemos el valor de la variable privada con el metodo getter.
	cout << objetoUno.getValue() << endl;

	return 0;
}