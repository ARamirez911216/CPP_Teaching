#include <iostream>
#include <string>

using namespace std;

class Mamifero{
	private:
		string nombre;
	public:
		void llamarMamifero(){
			cout << "El mamifero se llama: " << nombre << endl;
		}
		void setMamifero(string var){
			nombre = var;
		}
};

class Perro:public Mamifero{
	public:
		void show(){
		}
};

class Gato:public Mamifero{
	public:
		void show(){
		}
};

int main(){
	//Se observa como ambas clases heredan los metodos de la clase Mamifero
	//por lo que pueden hacer uso de ellos sin tener que reescribirlas en
	//cada una. Esto ahorra tiempo y codigo y permite organizar las
	//clases en grupos similares.
	Perro perroUno;
	perroUno.setMamifero("Fido");
	perroUno.llamarMamifero();

	Gato gatoUno;
	gatoUno.setMamifero("Pelusa");
	gatoUno.llamarMamifero();
	return 0;
}