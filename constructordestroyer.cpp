#include <iostream>
#include <string>
using namespace std;

class Condes{
	private:
		int valorInterno;
	public:
		//Constructor.
		Condes(){
			valorInterno = 5;
			cout<<"Constructor llamado. Valor interno: "<<valorInterno<<endl;
		}
		//Destructor.
		~Condes(){
			cout << "Destructor invocado para liberar memoria." << endl;
		}
		//Obtiene el valor de la variable privada valorInterno.
		int getValue(){
			return valorInterno;
		}

};

int main(){
	cout << "Hola soy el segundo tutorial de C++. Tema: constructores y destructores" <<endl;
	//Creamos un objeto de tipo Condes.
	//Automaticamente creara utilizando el constructor, un objeto de
	//tipo Condes con valor default de 5.
	Condes objetoUno;

	//Obtenemos el valor de la variable privada con el metodo getter.
	cout << objetoUno.getValue() << endl;

	return 0;
}